//
//  SmartGooseGameTests.swift
//  
//
//  Created by Massimiliano Bonafede on 23/05/24.
//

import XCTest
@testable import GooseGamePackage

final class SmartGooseGameTests: XCTestCase {
    
    func test_smartGooseGame_getJackpot() throws {
        let numberOfPlayer: Int = 6
        let post: Int = 200
        let expectedJackPot = numberOfPlayer * post
        let sut = try makeSUT(numberOfPlayers: numberOfPlayer, post: post)
        assertEqual(expectedJackPot, sut.getJackpot(), "Expected \(expectedJackPot)€ as jackpot, received \(sut.getJackpot()) instead.")
    }
    
    func test_smartGooseGame_emprtyPlayer() throws {
        XCTAssertThrowsError(try makeSUT(numberOfPlayers: 0)) { error in
            assertTrue(error is GameError, "Expected a GameError, received \(error) instead.")
        }
    }
    
    func test_smartGooseGame_currentPlayerIndex() throws {
        let sut = try makeSUT()
        let index = sut.currentPlayerIndex()
        let player = sut.players[index]
        assertTrue(player.currentStatus(), "Expected true, received false instead.")
    }
    
    func test_smartGooseGame_setNextPlayerAsCurrent() throws {
        for _ in 1 ... 10 {
            let sut = try makeSUT()
            let currentIndex = sut.currentPlayerIndex()
            sut.setNextPlayerAsCurrent()
            if currentIndex == sut.players.count - 1 {
                assertEqual(sut.currentPlayerIndex(), 0, "Expected 0 as index, received \(sut.currentPlayerIndex()) instead.")
            } else {
                let index = currentIndex + 1
                assertEqual(sut.currentPlayerIndex(), index, "Expected \(index) as index, received \(sut.currentPlayerIndex()) instead.")
            }
        }
    }
    
    func test_smartGooseGame_setCurrentPosition_playerStart0_rolls3_4_expected7() throws {
        let sut = try makeSUT()
        let expectation: Int = 7
        let (player, _) = sut.getCurrentAndOthers()
        player.setPosition(0)
        let position = sut.setPosition(position: player.getPosition(), rolls: (3, 4))
        assertEqual(position, expectation, "Expected \(expectation), received \(position) instead.")
    }
    
    func test_smartGooseGame_setCurrentPosition_playerStart10_rolls3_4_expected7() throws {
        let sut = try makeSUT()
        let expectation: Int = 17
        let (player, _) = sut.getCurrentAndOthers()
        player.setPosition(10)
        let position = sut.setPosition(position: player.getPosition(), rolls: (3, 4))
        assertEqual(position, expectation, "Expected \(expectation), received \(position) instead.")
    }
    
    func test_smartGooseGame_setCurrentPosition_playerStart51_rolls6_6_expected63() throws {
        let sut = try makeSUT()
        let expectation: Int = 63
        let (player, _) = sut.getCurrentAndOthers()
        player.setPosition(51)
        let position = sut.setPosition(position: player.getPosition(), rolls: (6, 6))
        assertEqual(position, expectation, "Expected \(expectation), received \(position) instead.")
    }
    
    func test_smartGooseGame_setCurrentPosition_playerStart52_rolls6_6_expected62() throws {
        let sut = try makeSUT()
        let expectation: Int = 62
        let (player, _) = sut.getCurrentAndOthers()
        player.setPosition(52)
        let position = sut.setPosition(position: player.getPosition(), rolls: (6, 6))
        assertEqual(position, expectation, "Expected \(expectation), received \(position) instead.")
    }
    
    func test_smartGooseGame_setCurrentPosition_playerStart55_rolls6_6_expected59() throws {
        let sut = try makeSUT()
        let expectation: Int = 59
        let (player, _) = sut.getCurrentAndOthers()
        player.setPosition(55)
        let position = sut.setPosition(position: player.getPosition(), rolls: (6, 6))
        assertEqual(position, expectation, "Expected \(expectation), received \(position) instead.")
    }
    
    func test_smartGooseGame_setCurrentPosition_playerStart62_rolls6_6_expected52() throws {
        let sut = try makeSUT()
        let expectation: Int = 52
        let (player, _) = sut.getCurrentAndOthers()
        player.setPosition(62)
        let position = sut.setPosition(position: player.getPosition(), rolls: (6, 6))
        assertEqual(position, expectation, "Expected \(expectation), received \(position) instead.")
    }
    
    func test_smartGooseGame_setCurrentPosition_playerStart62_rolls3_1_expected60() throws {
        let sut = try makeSUT()
        let expectation: Int = 60
        let (player, _) = sut.getCurrentAndOthers()
        player.setPosition(62)
        let position = sut.setPosition(position: player.getPosition(), rolls: (3, 1))
        assertEqual(position, expectation, "Expected \(expectation), received \(position) instead.")
    }
    
    func test_smartGooseGame_rollDice_playerPrisoner() throws {
        let sut = try makeSUT()
        let (player, _) = sut.getCurrentAndOthers()
        assertTrue(player.currentStatus(), "Expected true, received false instead.")
        player.setAs(prisoner: true)
        let expectation = expectation(description: "Waiting for completion.")
        sut.onRefreshCurrentPlayer = {
            self.assertFalse(player.currentStatus(), "Expected false, received true instead.")
            expectation.fulfill()
        }
        try sut.rollDice()
        wait(for: [expectation], timeout: 0.1)
    }
    
    func test_smartGooseGame_rollDice_playerTurnBlock() throws {
        let sut = try makeSUT()
        let (player, _) = sut.getCurrentAndOthers()
        assertTrue(player.currentStatus(), "Expected true, received false instead.")
        player.setAs(prisoner: true)
        let expectation = expectation(description: "Waiting for completion.")
        sut.onRefreshCurrentPlayer = {
            self.assertFalse(player.currentStatus(), "Expected false, received true instead.")
            expectation.fulfill()
        }
        try sut.rollDice()
        wait(for: [expectation], timeout: 0.1)
    }
    
    func test_smartGooseGame_rollsEvaluation_Start() throws {
        let sut = try makeSUT()
        let (player, _) = sut.getCurrentAndOthers()
        let expectation = expectation(description: "Waiting for completion.")
        sut.onRefreshCurrentPlayer = {
            self.assertFalse(player.currentStatus(), "Expected false, received true instead.")
            self.assertEqual(player.getPosition(), 0, "Expected 0, received \(player.getPosition()) instead.")
            self.assertFalse(player.currentStatus(), "Expected false, received true instead.")
            expectation.fulfill()
        }
        let rolls = (0, 0)
        try sut.rollsEvaluation(position: 0, rolls: rolls)
        wait(for: [expectation], timeout: 0.1)
    }
    
    func test_smartGooseGame_rollsEvaluation_Normal() throws {
        let sut = try makeSUT()
        let (player, _) = sut.getCurrentAndOthers()
        let expectation = expectation(description: "Waiting for completion.")
        let rolls = (1, 1)
        let position_expectation = player.getPosition() + rolls.0 + rolls.1
        sut.onRefreshCurrentPlayer = {
            self.assertFalse(player.currentStatus(), "Expected false, received true instead.")
            self.assertEqual(player.getPosition(), position_expectation, "Expected \(position_expectation), received \(player.getPosition()) instead.")
            self.assertFalse(player.currentStatus(), "Expected false, received true instead.")
            expectation.fulfill()
        }
        try sut.rollsEvaluation(position: 0, rolls: rolls)
        wait(for: [expectation], timeout: 0.1)
    }
    
    func test_smartGooseGame_rollsEvaluation_Goose() throws {
        let sut = try makeSUT()
        let (player, _) = sut.getCurrentAndOthers()
        let expectation = expectation(description: "Waiting for completion.")
        let rolls = (1, 1)
        player.setPosition(7)
        let position_expectation = player.getPosition() + 2 * (rolls.0 + rolls.1)
        sut.onRefreshCurrentPlayer = {
            self.assertFalse(player.currentStatus(), "Expected false, received true instead.")
            self.assertEqual(player.getPosition(), position_expectation, "Expected \(position_expectation), received \(player.getPosition()) instead.")
            self.assertFalse(player.currentStatus(), "Expected false, received true instead.")
            expectation.fulfill()
        }
        try sut.rollsEvaluation(position: player.getPosition(), rolls: rolls)
        wait(for: [expectation], timeout: 0.1)
    }
    
    func test_smartGooseGame_rollsEvaluation_SkeletonFromGoose() throws {
        let sut = try makeSUT()
        let (player, _) = sut.getCurrentAndOthers()
        let expectation = expectation(description: "Waiting for completion.")
        let rolls = (2, 2)
        player.setPosition(50)
        let position_expectation = 0
        sut.onRefreshCurrentPlayer = {
            self.assertFalse(player.currentStatus(), "Expected false, received true instead.")
            self.assertEqual(player.getPosition(), position_expectation, "Expected \(position_expectation), received \(player.getPosition()) instead.")
            self.assertFalse(player.currentStatus(), "Expected false, received true instead.")
            expectation.fulfill()
        }
        try sut.rollsEvaluation(position: player.getPosition(), rolls: rolls)
        wait(for: [expectation], timeout: 0.1)
    }
    
    func test_smartGooseGame_rollsEvaluation_LabyrinthFromGoose() throws {
        let sut = try makeSUT()
        let (player, _) = sut.getCurrentAndOthers()
        let expectation = expectation(description: "Waiting for completion.")
        let rolls = (3, 3)
        player.setPosition(30)
        let position_expectation = 39
        sut.onRefreshCurrentPlayer = {
            self.assertFalse(player.currentStatus(), "Expected false, received true instead.")
            self.assertEqual(player.getPosition(), position_expectation, "Expected \(position_expectation), received \(player.getPosition()) instead.")
            self.assertFalse(player.currentStatus(), "Expected false, received true instead.")
            expectation.fulfill()
        }
        try sut.rollsEvaluation(position: player.getPosition(), rolls: rolls)
        wait(for: [expectation], timeout: 0.1)
    }
    
    func test_smartGooseGame_rollsEvaluation_PrisonFromGoose() throws {
        let sut = try makeSUT()
        let (player, _) = sut.getCurrentAndOthers()
        let expectation = expectation(description: "Waiting for completion.")
        let rolls = (3, 4)
        player.setPosition(38)
        let position_expectation = 52
        sut.onRefreshCurrentPlayer = {
            self.assertFalse(player.currentStatus(), "Expected false, received true instead.")
            self.assertEqual(player.getPosition(), position_expectation, "Expected \(position_expectation), received \(player.getPosition()) instead.")
            self.assertFalse(player.currentStatus(), "Expected false, received true instead.")
            self.assertTrue(player.prisonerStatus(), "Expected true, received false instead.")
            expectation.fulfill()
        }
        try sut.rollsEvaluation(position: player.getPosition(), rolls: rolls)
        wait(for: [expectation], timeout: 0.1)
    }
    
    func test_smartGooseGame_rollsEvaluation_BridgeFromGoose() throws {
        let sut = try makeSUT()
        let (player, _) = sut.getCurrentAndOthers()
        let expectation = expectation(description: "Waiting for completion.")
        let rolls = (1, 1)
        player.setPosition(4)
        let position_expectation = 12
        sut.onRefreshCurrentPlayer = {
            self.assertFalse(player.currentStatus(), "Expected false, received true instead.")
            self.assertEqual(player.getPosition(), position_expectation, "Expected \(position_expectation), received \(player.getPosition()) instead.")
            self.assertFalse(player.currentStatus(), "Expected false, received true instead.")
            expectation.fulfill()
        }
        try sut.rollsEvaluation(position: player.getPosition(), rolls: rolls)
        wait(for: [expectation], timeout: 0.1)
    }
    
    func test_smartGooseGame_rollsEvaluation_House() throws {
        let sut = try makeSUT()
        let (player, _) = sut.getCurrentAndOthers()
        let expectation = expectation(description: "Waiting for completion.")
        let rolls = (1, 1)
        player.setPosition(17)
        let position_expectation = player.getPosition() + rolls.0 + rolls.1
        sut.onRefreshCurrentPlayer = {
            self.assertFalse(player.currentStatus(), "Expected false, received true instead.")
            self.assertEqual(player.getPosition(), position_expectation, "Expected \(position_expectation), received \(player.getPosition()) instead.")
            self.assertFalse(player.currentStatus(), "Expected false, received true instead.")
            self.assertTrue(player.turnBlockStatus(), "Expected true, received false instead.")
            expectation.fulfill()
        }
        try sut.rollsEvaluation(position: player.getPosition(), rolls: rolls)
        wait(for: [expectation], timeout: 0.1)
    }
    
    func test_smartGooseGame_rollsEvaluation_Labyrinth() throws {
        let sut = try makeSUT()
        let (player, _) = sut.getCurrentAndOthers()
        let expectation = expectation(description: "Waiting for completion.")
        let rolls = (1, 1)
        player.setPosition(40)
        let position_expectation = 39
        sut.onRefreshCurrentPlayer = {
            self.assertFalse(player.currentStatus(), "Expected false, received true instead.")
            self.assertEqual(player.getPosition(), position_expectation, "Expected \(position_expectation), received \(player.getPosition()) instead.")
            self.assertFalse(player.currentStatus(), "Expected false, received true instead.")
            expectation.fulfill()
        }
        try sut.rollsEvaluation(position: player.getPosition(), rolls: rolls)
        wait(for: [expectation], timeout: 0.1)
    }
    
    func test_smartGooseGame_rollsEvaluation_Skeleton() throws {
        let sut = try makeSUT()
        let (player, _) = sut.getCurrentAndOthers()
        let expectation = expectation(description: "Waiting for completion.")
        let rolls = (1, 1)
        player.setPosition(56)
        let position_expectation = 0
        sut.onRefreshCurrentPlayer = {
            self.assertFalse(player.currentStatus(), "Expected false, received true instead.")
            self.assertEqual(player.getPosition(), position_expectation, "Expected \(position_expectation), received \(player.getPosition()) instead.")
            self.assertFalse(player.currentStatus(), "Expected false, received true instead.")
            expectation.fulfill()
        }
        try sut.rollsEvaluation(position: player.getPosition(), rolls: rolls)
        wait(for: [expectation], timeout: 0.1)
    }
    
    func test_smartGooseGame_rollsEvaluation_Prison() throws {
        let sut = try makeSUT()
        let (player, _) = sut.getCurrentAndOthers()
        let expectation = expectation(description: "Waiting for completion.")
        let rolls = (1, 1)
        player.setPosition(50)
        let position_expectation = player.getPosition() + rolls.0 + rolls.1
        sut.onRefreshCurrentPlayer = {
            self.assertFalse(player.currentStatus(), "Expected false, received true instead.")
            self.assertEqual(player.getPosition(), position_expectation, "Expected \(position_expectation), received \(player.getPosition()) instead.")
            self.assertFalse(player.currentStatus(), "Expected false, received true instead.")
            self.assertTrue(player.prisonerStatus(), "Expected true, received false instead.")
            expectation.fulfill()
        }
        try sut.rollsEvaluation(position: player.getPosition(), rolls: rolls)
        wait(for: [expectation], timeout: 0.1)
    }
    
    func test_smartGooseGame_rollsEvaluation_PrisonMakeFreeOtherPrisoner() throws {
        let sut = try makeSUT()
        let (player, others) = sut.getCurrentAndOthers()
        let prisoner = others.randomElement()
        prisoner?.setAs(prisoner: true)
        assertTrue(prisoner!.prisonerStatus(), "Expected true, received false instead.")
        let expectation = expectation(description: "Waiting for completion.")
        let rolls = (1, 1)
        player.setPosition(50)
        let position_expectation = player.getPosition() + rolls.0 + rolls.1
        sut.onRefreshCurrentPlayer = {
            self.assertFalse(player.currentStatus(), "Expected false, received true instead.")
            self.assertEqual(player.getPosition(), position_expectation, "Expected \(position_expectation), received \(player.getPosition()) instead.")
            self.assertFalse(player.currentStatus(), "Expected false, received true instead.")
            self.assertTrue(player.prisonerStatus(), "Expected true, received false instead.")
            self.assertFalse(prisoner!.prisonerStatus(), "Expected false, received true instead.")
            expectation.fulfill()
        }
        try sut.rollsEvaluation(position: player.getPosition(), rolls: rolls)
        wait(for: [expectation], timeout: 0.1)
    }
    
    func test_smartGooseGame_rollsEvaluation_End() throws {
        let sut = try makeSUT()
        let (player, _) = sut.getCurrentAndOthers()
        let expectation = expectation(description: "Waiting for completion.")
        sut.onWinner = { [weak sut] result in
            self.assertEqual(result.winner.getPosition(), 0, "Expected 0, received \(result.winner.getPosition()) instead.")
            result.losers.forEach {
                self.assertEqual($0.getPosition(), 0, "Expected 0, received \($0.getPosition()) instead.")
            }
            self.assertEqual(sut!.getJackpot(), 0, "Expected 0, received \(sut!.getJackpot()) instead.")
            expectation.fulfill()
        }
        let rolls = (1, 1)
        player.setPosition(61)
        try sut.rollsEvaluation(position: player.getPosition(), rolls: rolls)
        wait(for: [expectation], timeout: 0.1)
    }
    
    func test_smartGooseGame_rollFirstLaunch3And6Expected26() throws {
        let sut = try makeSUT()
        let (player, _) = sut.getCurrentAndOthers()
        let expectation = expectation(description: "Waiting for completion.")
        sut.onRefreshCurrentPlayer = {
            self.assertFalse(player.currentStatus(), "Expected false, received true instead.")
            self.assertEqual(player.getPosition(), 26, "Expected 26, received \(player.getPosition()) instead.")
            expectation.fulfill()
        }
        try sut.roll(player: player, rolls: (3, 6))
        wait(for: [expectation], timeout: 0.1)
    }
    
    func test_smartGooseGame_rollFirstLaunch6And3Expected26() throws {
        let sut = try makeSUT()
        let (player, _) = sut.getCurrentAndOthers()
        let expectation = expectation(description: "Waiting for completion.")
        sut.onRefreshCurrentPlayer = {
            self.assertFalse(player.currentStatus(), "Expected false, received true instead.")
            self.assertEqual(player.getPosition(), 26, "Expected 26, received \(player.getPosition()) instead.")
            expectation.fulfill()
        }
        try sut.roll(player: player, rolls: (6, 3))
        wait(for: [expectation], timeout: 0.1)
    }
    
    func test_smartGooseGame_rollFirstLaunch4And5Expected53() throws {
        let sut = try makeSUT()
        let (player, _) = sut.getCurrentAndOthers()
        let expectation = expectation(description: "Waiting for completion.")
        sut.onRefreshCurrentPlayer = {
            self.assertFalse(player.currentStatus(), "Expected false, received true instead.")
            self.assertEqual(player.getPosition(), 53, "Expected 53, received \(player.getPosition()) instead.")
            expectation.fulfill()
        }
        try sut.roll(player: player, rolls: (4, 5))
        wait(for: [expectation], timeout: 0.1)
    }
    
    func test_smartGooseGame_rollFirstLaunch5And4Expected53() throws {
        let sut = try makeSUT()
        let (player, _) = sut.getCurrentAndOthers()
        let expectation = expectation(description: "Waiting for completion.")
        sut.onRefreshCurrentPlayer = {
            self.assertFalse(player.currentStatus(), "Expected false, received true instead.")
            self.assertEqual(player.getPosition(), 53, "Expected 53, received \(player.getPosition()) instead.")
            expectation.fulfill()
        }
        try sut.roll(player: player, rolls: (5, 4))
        wait(for: [expectation], timeout: 0.1)
    }
    
    // MARK: - Helper

    private func makeSUT(numberOfPlayers: Int = 4, post: Int = 100, file: StaticString = #filePath, line: UInt = #line) throws -> SmartGooseGame {
        let squareBuilder = SquareBuilder()
        let sut = try SmartGooseGame(players: players(numberOfPlayers), squareBuilder: squareBuilder, post: post)
        trackForMemoryLeaks(sut, file: file, line: line)
        return sut
    }
    
    private func players(_ numbers: Int) -> [Player] {
        guard numbers > 0 else { return [] }
        var players: [Player] = []
        for i in 1 ... numbers {
            players.append(Player(name: "Player_\(i)"))
        }
        return players
    }
}
