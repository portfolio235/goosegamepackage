//
//  SquareBuilderTests.swift
//  
//
//  Created by Massimiliano Bonafede on 23/05/24.
//

import XCTest
@testable import GooseGamePackage

final class SquareBuilderTests: XCTestCase {
    
    func test_squareOfTypeStart() throws {
        try assertNumber(startSquareNumber(), for: .start)
    }
    
    func test_squareOfTypeNormal() throws {
        try assertAllNumbers(normalSquareNumbers()) { position in
            return .normal(position: position)
        }
    }
    
    func test_squareOfTypeGame() throws {
        try assertAllNumbers(gooseSquareNumbers()) { position in
            return .goose(position: position)
        }
    }
    
    func test_squareOfTypeBridge() throws {
        try assertNumber(bridgeSquareNumber(), for: .bridge(position: bridgeSquareNumber()))
    }
    
    func test_squareOfTypeHouse() throws {
        try assertNumber(houseSquareNumber(), for: .house(position: houseSquareNumber()))
    }
    
    func test_squareOfTypePrison() throws {
        try assertAllNumbers(prisonSquareNumbers()) { position in
            return .prison(position: position)
        }
    }
    
    func test_squareOfTypeLabyrinth() throws {
        try assertNumber(labyrinthSquareNumber(), for: .labyrinth(position: labyrinthSquareNumber()))
    }
    
    func test_squareOfTypeSkeleton() throws {
        try assertNumber(skeletonSquareNumber(), for: .skeleton(position: skeletonSquareNumber()))
    }
    
    func test_squareOfTypeEnd() throws {
        try assertNumber(endSquareNumber(), for: .end)
    }

    // MARK: - Helper

    private func makeSUT(file: StaticString = #filePath, line: UInt = #line) -> SquareBuilder {
        let sut = SquareBuilder()
        trackForMemoryLeaks(sut, file: file, line: line)
        return sut
    }
    
    private func assert(at square: Square, number: Int, type: SquareType, file: StaticString = #filePath, line: UInt = #line) {
        assertEqual(square.number,
                    number,
                    "Expected \(square.number), received \(number) instead.",
                    file: file,
                    line: line)
        assertEqual(square.type,
                    type,
                    "Expected \(square.type) with number \(square.number), received \(type) insted.",
                    file: file,
                    line: line)
    }
    
    private func assertAllNumbers(_ numbers: [Int], compare: (Int) -> SquareType, file: StaticString = #filePath, line: UInt = #line) throws {
        let sut = makeSUT()
        for number in numbers {
            let square = try sut.getSquare(at: number)
            let type = compare(number)
            assert(at: square, number: number, type: type, file: file, line: line)
        }
    }
    
    private func assertNumber(_ number: Int, for type: SquareType, file: StaticString = #filePath, line: UInt = #line) throws {
        let sut = makeSUT()
        let square = try sut.getSquare(at: number)
        assert(at: square, number: number, type: type, file: file, line: line)
    }
}
