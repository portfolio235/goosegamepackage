//
//  DiceTests.swift
//  
//
//  Created by Massimiliano Bonafede on 23/05/24.
//

import XCTest
@testable import GooseGamePackage

final class DiceTests: XCTestCase {

    func test_rollDice() throws {
        let sut = makeSUT()
        for _ in 1 ... 20 {
            let number = sut.roll()
            try assert(number)
        }
    }
    
    func test_rollDice_wrongNumber() throws {
        XCTAssertThrowsError(try assert(0))
    }
    
    // MARK: - Helper

    private func makeSUT(file: StaticString = #filePath, line: UInt = #line) -> Dice {
        let sut = Dice()
        trackForMemoryLeaks(sut, file: file, line: line)
        return sut
    }
    
    private func assert(_ number: Int, file: StaticString = #filePath, line: UInt = #line) throws {
        let condiction = number == 0 || number > 6
        if condiction {
            throw NSError(domain: "Number \(number) is not a valid value of a dice.", code: -1)
        } else {
            assertTrue(!condiction, "Number \(number) is not a valid value of a dice.")
        }
    }
}
