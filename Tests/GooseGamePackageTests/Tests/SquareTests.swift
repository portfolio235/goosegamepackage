//
//  SquareTests.swift
//  
//
//  Created by Massimiliano Bonafede on 23/05/24.
//

import XCTest
@testable import GooseGamePackage

final class SquareTests: XCTestCase {

    func test_square() {
        let typeExpectation: SquareType = .start
        let numberExpectation: Int = 0
        let sut = makeSUT(type: typeExpectation, number: numberExpectation)
        assertEqual(sut.type, typeExpectation, "Expected \(typeExpectation), received \(sut.type) instead.")
        assertEqual(sut.number, numberExpectation, "Expected \(numberExpectation), received \(sut.number) instead.")
    }
    
    func test_square_LHSStartEqualToRHSStart() {
        let lhs_start: Square = Square(type: .start, number: 0)
        let rhs_start: Square = Square(type: .start, number: 0)
        assertEqual(lhs_start, rhs_start, "Expected squares equal.")
    }
    
    func test_square_LHSNormalEqualToRHSNormal() {
        let lhs_start: Square = Square(type: .normal(position: 2), number: 0)
        let rhs_start: Square = Square(type: .normal(position: 2), number: 0)
        assertEqual(lhs_start, rhs_start, "Expected squares equal.")
    }
    
    func test_square_LHSNormalNotEqualToRHSNormal() {
        let lhs_start: Square = Square(type: .normal(position: 2), number: 0)
        let rhs_start: Square = Square(type: .normal(position: 23), number: 0)
        assertNotEqual(lhs_start, rhs_start, "Expected squares equal.")
    }
    
    func test_square_LHSGooseEqualToRHSGoose() {
        let lhs_start: Square = Square(type: .goose(position: 5), number: 0)
        let rhs_start: Square = Square(type: .goose(position: 5), number: 0)
        assertEqual(lhs_start, rhs_start, "Expected squares equal.")
    }
    
    func test_square_LHSGooseNotEqualToRHSGoose() {
        let lhs_start: Square = Square(type: .goose(position: 5), number: 0)
        let rhs_start: Square = Square(type: .goose(position: 15), number: 0)
        assertNotEqual(lhs_start, rhs_start, "Expected squares equal.")
    }
    
    func test_square_LHSBridgeEqualToRHSBridge() {
        let lhs_start: Square = Square(type: .bridge(position: 6), number: 0)
        let rhs_start: Square = Square(type: .bridge(position: 6), number: 0)
        assertEqual(lhs_start, rhs_start, "Expected squares equal.")
    }
    
    func test_square_LHSBridgeNotEqualToRHSBridge() {
        let lhs_start: Square = Square(type: .bridge(position: 6), number: 0)
        let rhs_start: Square = Square(type: .bridge(position: 16), number: 0)
        assertNotEqual(lhs_start, rhs_start, "Expected squares equal.")
    }
    
    func test_square_LHSHouseEqualToRHSHouse() {
        let lhs_start: Square = Square(type: .house(position: 19), number: 0)
        let rhs_start: Square = Square(type: .house(position: 19), number: 0)
        assertEqual(lhs_start, rhs_start, "Expected squares equal.")
    }
    
    func test_square_LHSHouseNotEqualToRHSHouse() {
        let lhs_start: Square = Square(type: .house(position: 19), number: 0)
        let rhs_start: Square = Square(type: .house(position: 9), number: 0)
        assertNotEqual(lhs_start, rhs_start, "Expected squares equal.")
    }
    
    func test_square_LHSPrisonEqualToRHSPrison() {
        let lhs_start: Square = Square(type: .prison(position: 31), number: 0)
        let rhs_start: Square = Square(type: .prison(position: 31), number: 0)
        assertEqual(lhs_start, rhs_start, "Expected squares equal.")
    }
    
    func test_square_LHSPrisonNotEqualToRHSPrison() {
        let lhs_start: Square = Square(type: .prison(position: 31), number: 0)
        let rhs_start: Square = Square(type: .prison(position: 1), number: 0)
        assertNotEqual(lhs_start, rhs_start, "Expected squares equal.")
    }
    
    func test_square_LHSLabyrinthEqualToRHSLabyrinth() {
        let lhs_start: Square = Square(type: .labyrinth(position: 42), number: 0)
        let rhs_start: Square = Square(type: .labyrinth(position: 42), number: 0)
        assertEqual(lhs_start, rhs_start, "Expected squares equal.")
    }
    
    func test_square_LHSLabyrinthNotEqualToRHSLabyrinth() {
        let lhs_start: Square = Square(type: .labyrinth(position: 2), number: 0)
        let rhs_start: Square = Square(type: .labyrinth(position: 42), number: 0)
        assertNotEqual(lhs_start, rhs_start, "Expected squares equal.")
    }
    
    func test_square_LHSSkeletonEqualToRHSSkeleton() {
        let lhs_start: Square = Square(type: .skeleton(position: 58), number: 0)
        let rhs_start: Square = Square(type: .skeleton(position: 58), number: 0)
        assertEqual(lhs_start, rhs_start, "Expected squares equal.")
    }
    
    func test_square_LHSSkeletonNotEqualToRHSSkeleton() {
        let lhs_start: Square = Square(type: .skeleton(position: 8), number: 0)
        let rhs_start: Square = Square(type: .skeleton(position: 58), number: 0)
        assertNotEqual(lhs_start, rhs_start, "Expected squares equal.")
    }
    
    func test_square_LHSEndEqualToRHSEnd() {
        let lhs_start: Square = Square(type: .end, number: 0)
        let rhs_start: Square = Square(type: .end, number: 0)
        assertEqual(lhs_start, rhs_start, "Expected squares equal.")
    }
    
    func test_square_LHSNotEqualToRHS() {
        let lhs_start: Square = Square(type: .prison(position: 8), number: 0)
        let rhs_start: Square = Square(type: .skeleton(position: 8), number: 0)
        assertNotEqual(lhs_start, rhs_start, "Expected squares equal.")
    }
    
    func test_square_LHSNotEqualNumberToRHS() {
        let lhs_start: Square = Square(type: .prison(position: 8), number: 10)
        let rhs_start: Square = Square(type: .skeleton(position: 8), number: 0)
        assertNotEqual(lhs_start, rhs_start, "Expected squares equal.")
    }
    
    // MARK: - Helper

    private func makeSUT(type: SquareType, number: Int, file: StaticString = #filePath, line: UInt = #line) -> Square {
        let sut = Square(type: type, number: number)
        trackForMemoryLeaks(sut, file: file, line: line)
        return sut
    }
}
