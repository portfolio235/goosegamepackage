//
//  LocalizableTests.swift
//  
//
//  Created by Massimiliano Bonafede on 23/05/24.
//

import XCTest
@testable import GooseGamePackage

final class LocalizableTests: XCTestCase {

    func test_locale_start() throws {
        //let string = NSLocalizedString("greating", tableName: "Localizable", bundle: Bundle.module, comment: "Not found")
        print("")
        let sut = makeSUT()
        let results = try sut.check("sqare.start.description")
        for result in results {
            assertTrue(result.isPresent, "Missing \(result.key) on \(result.locale).lproj")
        }
    }
    
    func test_locale_normal() throws {
        let sut = makeSUT()
        let results = try sut.check("sqare.normal.description")
        for result in results {
            assertTrue(result.isPresent, "Missing \(result.key) on \(result.locale).lproj")
        }
    }
    
    func test_locale_goose() throws {
        let sut = makeSUT()
        let results = try sut.check("sqare.goose.description")
        for result in results {
            assertTrue(result.isPresent, "Missing \(result.key) on \(result.locale).lproj")
        }
    }
    
    func test_locale_bridge() throws {
        let sut = makeSUT()
        let results = try sut.check("sqare.bridge.description")
        for result in results {
            assertTrue(result.isPresent, "Missing \(result.key) on \(result.locale).lproj")
        }
    }
    
    func test_locale_house() throws {
        let sut = makeSUT()
        let results = try sut.check("sqare.house.description")
        for result in results {
            assertTrue(result.isPresent, "Missing \(result.key) on \(result.locale).lproj")
        }
    }
    
    func test_locale_prison() throws {
        let sut = makeSUT()
        let results = try sut.check("sqare.prison.description")
        for result in results {
            assertTrue(result.isPresent, "Missing \(result.key) on \(result.locale).lproj")
        }
    }
    
    func test_locale_prisonWithPrisoner() throws {
        let sut = makeSUT()
        let results = try sut.check("sqare.prison.description.prisoner")
        for result in results {
            assertTrue(result.isPresent, "Missing \(result.key) on \(result.locale).lproj")
        }
    }
    
    func test_locale_labyrinth() throws {
        let sut = makeSUT()
        let results = try sut.check("sqare.labyrinth.description")
        for result in results {
            assertTrue(result.isPresent, "Missing \(result.key) on \(result.locale).lproj")
        }
    }
    
    func test_locale_skeleton() throws {
        let sut = makeSUT()
        let results = try sut.check("sqare.skeleton.description")
        for result in results {
            assertTrue(result.isPresent, "Missing \(result.key) on \(result.locale).lproj")
        }
    }
    
    func test_locale_end() throws {
        let sut = makeSUT()
        let results = try sut.check("sqare.end.description")
        for result in results {
            assertTrue(result.isPresent, "Missing \(result.key) on \(result.locale).lproj")
        }
    }

    // MARK: - Helper

    private func makeSUT(file: StaticString = #filePath, line: UInt = #line) -> LocalizableHelper {
        let sut = LocalizableHelper()
        trackForMemoryLeaks(sut, file: file, line: line)
        return sut
    }
}
