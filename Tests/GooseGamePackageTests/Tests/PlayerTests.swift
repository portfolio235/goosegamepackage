//
//  PlayerTests.swift
//  
//
//  Created by Massimiliano Bonafede on 23/05/24.
//

import XCTest
@testable import GooseGamePackage

final class PlayerTests: XCTestCase {

    func test_player_initWithName() {
        let name: String = "John"
        let sut = makeSUT(name: name)
        assertEqual(sut.name, name, "Expected \(name), received \(sut.name) instead,")
    }
    
    func test_player_startPositionIsZero() {
        let sut = makeSUT()
        assertEqual(sut.getPosition(), 0, "Expected 0, received \(sut.getPosition()) instead.")
    }
    
    func test_player_roll() {
        let sut = makeSUT()
        let range = 1 ... 6
        let (roll1, roll2) = sut.roll()
        assertTrue(range.contains(roll1), "Expected number in a range from 1 to 6, received \(roll1)")
        assertTrue(range.contains(roll2), "Expected number in a range from 1 to 6, received \(roll2)")
    }
    
    func test_player_setPosition() {
        let sut = makeSUT()
        let position = Int.random(in: 0 ... 63)
        sut.setPosition(position)
        assertEqual(sut.getPosition(), position, "Expected \(position), received \(sut.getPosition()) instead.")
    }
    
    func test_player_getPosition() {
        let sut = makeSUT()
        let position = Int.random(in: 0 ... 63)
        sut.setPosition(position)
        assertEqual(sut.getPosition(), position, "Expected \(position), received \(sut.getPosition()) instead.")
    }
    
    func test_player_addToPosition() {
        let sut = makeSUT()
        let startPosition = Int.random(in: 0 ... 30)
        let endPosition = Int.random(in: 31 ... 63)
        let sum = startPosition + endPosition
        sut.setPosition(startPosition)
        sut.addToPosition(endPosition)
        assertEqual(sut.getPosition(), sum, "Expected \(sum), received \(sut.getPosition()) instead.")
    }
    
    func test_player_removeToPosition() {
        let sut = makeSUT()
        let startPosition = Int.random(in: 0 ... 30)
        let endPosition = Int.random(in: 31 ... 63)
        let removePosition = Int.random(in: 0 ... 17)
        let sum = startPosition + endPosition - removePosition
        sut.setPosition(startPosition)
        sut.addToPosition(endPosition)
        sut.removeToPosition(removePosition)
        assertEqual(sut.getPosition(), sum, "Expected \(sum), received \(sut.getPosition()) instead.")
    }
    
    func test_player_getIsFirstLaunchTrue() {
        let sut = makeSUT()
        assertTrue(sut.getIsFirstLaunch(), "Expected true, received false instead")
    }
    
    func test_player_getIsFirstLaunchFalse() {
        let sut = makeSUT()
        sut.setAsPassedFirstLaunch()
        assertFalse(sut.getIsFirstLaunch(), "Expected false, received true instead")
    }
    
    func test_player_setAsPrisonerTrue() {
        let sut = makeSUT()
        sut.setAs(prisoner: true)
        assertTrue(sut.prisonerStatus(), "Expected true, received false instead.")
    }
    
    func test_player_setAsPrisonerFalse() {
        let sut = makeSUT()
        sut.setAs(prisoner: false)
        assertFalse(sut.prisonerStatus(), "Expected false, received true instead.")
    }
    
    func test_player_setAsCurrentTrue() {
        let sut = makeSUT()
        sut.setAs(current: true)
        assertTrue(sut.currentStatus(), "Expected true, received false instead.")
    }
    
    func test_player_setAsCurrentFalse() {
        let sut = makeSUT()
        sut.setAs(current: false)
        assertFalse(sut.currentStatus(), "Expected false, received true instead.")
    }
    
    func test_player_setAsTurnBockTrue() {
        let sut = makeSUT()
        sut.setAs(turnBlock: true)
        assertTrue(sut.turnBlockStatus(), "Expected true, received false instead.")
    }
    
    func test_player_setAsTurnBockFalse() {
        let sut = makeSUT()
        sut.setAs(turnBlock: false)
        assertFalse(sut.turnBlockStatus(), "Expected false, received true instead.")
    }
    
    func test_player_setName() {
        let name: String = "Jack"
        let exected_name: String = "Bob"
        let sut = makeSUT(name: name)
        sut.set(name: exected_name)
        assertEqual(sut.getName(), exected_name, "Expected \(exected_name), received \(sut.getName()) instead.")
    }
    
    func test_player_LHSEqualToRHS() {
        let name: String = "A name"
        let squareDescription: String = "A description"
        let position: Int = 6
        let isFirstLaunch: Bool  = Bool.random()
        let isPrisoner: Bool = Bool.random()
        let isCurrentPlayer: Bool = Bool.random()
        let isTurnBlock: Bool = Bool.random()
        let player1 = create(player: name,
                             squareDescription: squareDescription,
                             position: position,
                             isFirstLaunch: isFirstLaunch,
                             isPrisoner: isPrisoner,
                             isCurrentPlayer: isCurrentPlayer,
                             isTurnBlock: isTurnBlock)
        let player2 = player1
        assertEqual(player1, player2, "Expected \(player1) and \(player2) been equals.")
    }
    
    func test_player_LHSNotEqualToRHS() {
        let name: String = "A name"
        let squareDescription: String = "A description"
        let position: Int = 6
        let isFirstLaunch: Bool  = Bool.random()
        let isPrisoner: Bool = Bool.random()
        let isCurrentPlayer: Bool = Bool.random()
        let isTurnBlock: Bool = Bool.random()
        let player1 = create(player: name,
                             squareDescription: squareDescription,
                             position: position,
                             isFirstLaunch: isFirstLaunch,
                             isPrisoner: isPrisoner,
                             isCurrentPlayer: isCurrentPlayer,
                             isTurnBlock: isTurnBlock)
        let player2 = create(player: name,
                             squareDescription: squareDescription,
                             position: position,
                             isFirstLaunch: isFirstLaunch,
                             isPrisoner: isPrisoner,
                             isCurrentPlayer: isCurrentPlayer,
                             isTurnBlock: isTurnBlock)
        assertNotEqual(player1, player2, "Expected \(player1) and \(player2) been not equals.")
    }
    
    // MARK: - Helper

    private func makeSUT(name: String = "John", file: StaticString = #filePath, line: UInt = #line) -> Player {
        let sut = Player(name: name)
        trackForMemoryLeaks(sut, file: file, line: line)
        return sut
    }
    
    private func create(player name: String, squareDescription: String, position: Int, isFirstLaunch: Bool, isPrisoner: Bool, isCurrentPlayer: Bool, isTurnBlock: Bool) -> Player {
        let player = Player(name: name)
        player.squareDescription = squareDescription
        player.setPosition(position)
        if isFirstLaunch == false {
            player.setAsPassedFirstLaunch()
        }
        player.setAs(prisoner: isPrisoner)
        player.setAs(turnBlock: isTurnBlock)
        return player
    }
}

