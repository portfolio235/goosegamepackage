//
//  XCTestCase+Extensions.swift
//  
//
//  Created by Massimiliano Bonafede on 23/05/24.
//

import XCTest

extension XCTestCase {
    func trackForMemoryLeaks(_ instance: AnyObject, file: StaticString = #filePath, line: UInt = #line) {
        addTeardownBlock { [weak instance] in
            XCTAssertNil(instance, "Instance should have been deallocated. Potential memory leak.", file: file, line: line)
        }
    }
    
    func assertTrue(_ expression: Bool, _ message: String, file: StaticString = #filePath, line: UInt = #line) {
        XCTAssertTrue(expression, message, file: file, line: line)
    }
    
    func assertFalse(_ expression: Bool, _ message: String, file: StaticString = #filePath, line: UInt = #line) {
        XCTAssertFalse(expression, message, file: file, line: line)
    }
    
//    func assertFail(_ message: String, file: StaticString = #filePath, line: UInt = #line) {
//        XCTFail(message, file: file, line: line)
//    }
    
    func assertEqual<T>(_ expression1: T, _ expression2: T, _ message: String, file: StaticString = #filePath, line: UInt = #line) where T : Equatable {
        XCTAssertEqual(expression1, expression2, message, file: file, line: line)
    }
    
    func assertNotEqual<T>(_ expression1: T, _ expression2: T, _ message: String, file: StaticString = #filePath, line: UInt = #line) where T : Equatable {
        XCTAssertNotEqual(expression1, expression2, message, file: file, line: line)
    }
    
    func assert(given: () -> Void, when: () -> Void, then: () -> Void) {
        given()
        when()
        then()
    }
}

