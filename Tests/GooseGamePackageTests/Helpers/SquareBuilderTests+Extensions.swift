//
//  SquareBuilderTests+Extensions.swift
//  
//
//  Created by Massimiliano Bonafede on 23/05/24.
//

import XCTest

extension SquareBuilderTests {
    func startSquareNumber() -> Int { return 0 }
    func normalSquareNumbers() -> [Int] {
        return [1, 2, 3, 4, 7, 8, 10,
                11, 12, 13, 14, 15, 16, 17,
                20, 21, 22, 23, 24, 25, 26, 28, 29,
                30, 32, 33, 34, 35, 37, 38, 39,
                40, 41, 43, 44, 46, 47, 48, 49,
                50, 51, 53, 55, 56, 57, 59,
                60, 61, 62]
    }
    func gooseSquareNumbers() -> [Int] {
        return [5, 9, 18, 27, 36, 45, 54]
    }
    func bridgeSquareNumber() -> Int { return 6 }
    func houseSquareNumber() -> Int { return 19 }
    func prisonSquareNumbers() -> [Int] { return [31, 52] }
    func labyrinthSquareNumber() -> Int { return 42 }
    func skeletonSquareNumber() -> Int { return 58 }
    func endSquareNumber() -> Int { return 63 }
}
