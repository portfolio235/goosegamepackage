//
//  Square.swift
//
//
//  Created by Massimiliano Bonafede on 23/05/24.
//

import Foundation

public class Square {
    
    // MARK: - Properties

    let type: SquareType
    let number: Int
    
    // MARK: - Lifecycle

    public init(type: SquareType, number: Int) {
        self.type = type
        self.number = number
    }
}

extension Square: Equatable {
    static public func == (lhs: Square, rhs: Square) -> Bool {
        guard lhs.number == rhs.number else { return false }
        switch (lhs.type, rhs.type) {
        case (.start, .start):
            return true
        case let (.normal(position: lhs_position), .normal(position: rhs_position)):
            if lhs_position == rhs_position {
                return true
            } else {
                return false
            }
        case let (.goose(position: lhs_position), .goose(position: rhs_position)):
            if lhs_position == rhs_position {
                return true
            } else {
                return false
            }
        case let (.bridge(position: lhs_position), .bridge(position: rhs_position)):
            if lhs_position == rhs_position {
                return true
            } else {
                return false
            }
        case let (.house(position: lhs_position), .house(position: rhs_position)):
            if lhs_position == rhs_position {
                return true
            } else {
                return false
            }
        case let (.prison(position: lhs_position), .prison(position: rhs_position)):
            if lhs_position == rhs_position {
                return true
            } else {
                return false
            }
        case let (.labyrinth(position: lhs_position), .labyrinth(position: rhs_position)):
            if lhs_position == rhs_position {
                return true
            } else {
                return false
            }
        case let (.skeleton(position: lhs_position), .skeleton(position: rhs_position)):
            if lhs_position == rhs_position {
                return true
            } else {
                return false
            }
        case (.end, .end):
            return true
        default:
            return false
        }
    }
}
