//
//  GameError.swift
//
//
//  Created by Massimiliano Bonafede on 23/05/24.
//

import Foundation

public enum GameError: Error {
    case indexOutOfRange
    case gameIsEnded(winner: Player)
    case emptyPlayers
}
