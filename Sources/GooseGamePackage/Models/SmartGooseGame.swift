//
//  SmartGooseGame.swift
//
//
//  Created by Massimiliano Bonafede on 27/05/24.
//

import Foundation

final public class SmartGooseGame {
    
    public typealias WINNER = (winner: Player, losers: [Player], jackpot: Int)
    
    // MARK: - Properties

    public let players: [Player]
    public let squareBuilder: SquareBuilder
    private let post: Int
    private var jackpot: Int
    public var onRefreshCurrentPlayer: (() -> Void)? = nil
    public var onWinner: ((WINNER) -> Void)? = nil
    
    // MARK: - Lifecycle
    
    init(players: [Player], squareBuilder: SquareBuilder, post: Int) throws {
        self.players = players
        self.squareBuilder = squareBuilder
        self.post = post
        self.jackpot = post * players.count
        try assignStartPlayer()
    }
    
    // MARK: - Methods
    
    func getJackpot() -> Int {
        return jackpot
    }
    
    func assignStartPlayer() throws {
        if let player = players.randomElement() {
            player.setAs(current: true)
        } else {
            throw GameError.emptyPlayers
        }
    }
    
    func currentPlayerIndex() -> Int {
        var index = 0
        for player in players {
            if player.currentStatus() {
                return index
            }
            index += 1
        }
        return index
    }
    
    func setNextPlayerAsCurrent() {
        let index = currentPlayerIndex()
        if index < players.count - 1 {
            players[index].setAs(current: false)
            players[index + 1].setAs(current: true)
        } else {
            players[index].setAs(current: false)
            players[0].setAs(current: true)
        }
    }
    
    func getCurrentAndOthers() -> (player: Player, players: [Player]) {
        let current = players[currentPlayerIndex()]
        let others = players.filter { $0.uuid != current.uuid }
        return (current, others)
    }
    
    func setPosition(position: Int, rolls: (Int, Int)) -> Int {
        let total = position + rolls.0 + rolls.1
        if total <= 63 {
            return total
        } else {
            let offset = total - 63
            return 63 - offset
        }
    }
    
    func squareCalculation(position: Int, rolls: (Int, Int)) throws -> SquareType {
        let position = setPosition(position: position, rolls: rolls)
        return try squareBuilder.getSquare(at: position).type
    }
    
    func rollDice() throws {
        let (player, _) = getCurrentAndOthers()
        guard player.prisonerStatus() == false && player.turnBlockStatus() == false else {
            setNextPlayerAsCurrent()
            onRefreshCurrentPlayer?()
            return
        }
        let rolls = player.roll()
        try roll(player: player, rolls: rolls)
    }
    
    func roll(player: Player, rolls: (Int, Int)) throws {
        if player.getIsFirstLaunch(), rolls == (3, 6) || rolls == (6, 3) {
            manageNormalSquare(26)
        } else if player.getIsFirstLaunch(), rolls == (4, 5) || rolls == (5, 4) {
            manageNormalSquare(53)
        } else {
            try rollsEvaluation(position: player.getPosition(), rolls: rolls)
        }
    }
    
    func rollsEvaluation(position: Int, rolls: (Int, Int)) throws {
        let square = try squareCalculation(position: position, rolls: rolls)
        switch square {
        case .start:
            manageStartSquare()
        case let .normal(position):
            manageNormalSquare(position)
        case let .goose(position):
            try manageGooseSquare(position, rolls: rolls)
        case .bridge:
            manageBridgeSquare()
        case let .house(position):
            manageHouseSquare(position)
        case let .prison(position):
            managePrisonSquare(position)
        case .labyrinth:
            manageLabyrinthSquare()
        case .skeleton:
            manageSkeletonSquare()
        case .end:
            manageEndSquare()
        }
    }
    
    private func manageStartSquare() {
        let (player, _) = getCurrentAndOthers()
        player.setPosition(0)
        player.squareDescription = "sqare.start.description".localized()
        setNextPlayerAsCurrent()
        onRefreshCurrentPlayer?()
    }
    
    private func manageNormalSquare(_ position: Int) {
        let (player, _) = getCurrentAndOthers()
        player.addToPosition(position)
        player.squareDescription = "sqare.normal.description".localized()
        setNextPlayerAsCurrent()
        onRefreshCurrentPlayer?()
    }
    
    private func manageGooseSquare(_ position: Int, rolls: (Int, Int)) throws {
        let (player, _) = getCurrentAndOthers()
        let newPosition = position + rolls.0 + rolls.1
        player.setPosition(newPosition)
        let square = try squareBuilder.getSquare(at: newPosition).type
        switch square {
        case let .prison(position):
            managePrisonSquare(position)
        case .labyrinth:
            manageLabyrinthSquare()
        case .skeleton:
            manageSkeletonSquare()
        default:
            player.squareDescription = "sqare.goose.description".localized()
            setNextPlayerAsCurrent()
            onRefreshCurrentPlayer?()
        }
    }
    
    private func manageBridgeSquare() {
        let (player, _) = getCurrentAndOthers()
        player.setPosition(12)
        jackpot += post
        player.squareDescription = String(format: "sqare.bridge.description".localized(), post)
        setNextPlayerAsCurrent()
        onRefreshCurrentPlayer?()
    }
    
    private func manageHouseSquare(_ position: Int) {
        let (player, _) = getCurrentAndOthers()
        player.setPosition(position)
        player.setAs(turnBlock: true)
        player.squareDescription = String(format: "sqare.house.description".localized(), post)
        setNextPlayerAsCurrent()
        onRefreshCurrentPlayer?()
    }
    
    private func managePrisonSquare(_ position: Int) {
        let (player, others) = getCurrentAndOthers()
        player.setPosition(position)
        player.setAs(prisoner: true)
        let prisoner = others.first(where: { $0.prisonerStatus() })
        prisoner?.setAs(prisoner: false)
        let type = position == 31 ? "POZZO" : "PRIGIONE"
        if let prisoner = prisoner {
            player.squareDescription = String(format: "sqare.prison.description.prisoner".localized(),"\(type)", "\(prisoner)")
        } else {
            player.squareDescription = String(format: "sqare.prison.description".localized(), type)
        }
        setNextPlayerAsCurrent()
        onRefreshCurrentPlayer?()
    }
    
    private func manageLabyrinthSquare() {
        let (player, _) = getCurrentAndOthers()
        player.setPosition(39)
        jackpot += post
        player.squareDescription = String(format: "sqare.labyrinth.description".localized(), post)
        setNextPlayerAsCurrent()
        onRefreshCurrentPlayer?()
    }
    
    private func manageSkeletonSquare() {
        let (player, _) = getCurrentAndOthers()
        player.setPosition(0)
        jackpot += post
        player.squareDescription = String(format: "sqare.skeleton.description".localized(), post)
        setNextPlayerAsCurrent()
        onRefreshCurrentPlayer?()
    }
    
    private func manageEndSquare() {
        let (player, others) = getCurrentAndOthers()
        player.setPosition(0)
        others.forEach { $0.setPosition(0) }
        player.squareDescription = String(format: "sqare.end.description".localized(), jackpot)
        jackpot = 0
        onWinner?((player, others, jackpot))
    }
}
