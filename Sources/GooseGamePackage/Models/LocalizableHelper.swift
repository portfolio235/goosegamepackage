//
//  LocalizableHelper.swift
//
//
//  Created by Massimiliano Bonafede on 23/05/24.
//

import Foundation

class LocalizableHelper {
    
    typealias Result = (locale: String, key: String, isPresent: Bool)
    
    // MARK: - Properties

    private var localizations: [String] {
        return Bundle.module.localizations
    }
    
    // MARK: - Lifecycle

    init() {}
    
    // MARK: - Methods
    
    func check(_ key: String) throws -> [Result] {
        var results: [Result] = []
        for locale in localizations {
            let result = try check(locale, for: key)
            results.append(result)
        }
        return results
    }
    
    private func check(_ locale: String, for key: String) throws -> Result {
        guard let path = Bundle.module.path(forResource: locale, ofType: "lproj") else {
            throw NSError(domain: "Missing locale path for \(locale)", code: -1)
        }
        let bundle = Bundle(path: path)
        let missed = "MISSED"
        if let string = bundle?.localizedString(forKey: key, value: missed, table: nil), string != missed {
            return (locale, key, true)
        } else {
            return (locale, key, false)
        }
    }
}

extension String {
    func localized(bundle: Bundle = .module, tableName: String = "Localizable") -> String {
        return bundle.localizedString(forKey: self, value: nil, table: tableName)
    }
}
