//
//  Player.swift
//
//
//  Created by Massimiliano Bonafede on 23/05/24.
//

import Foundation

public class Player {
        
    // MARK: - Properties

    public let uuid: UUID = UUID()
    private(set) var name: String
    private var position: Int = 0
    public var squareDescription: String? = nil
    private(set) var isFirstLaunch: Bool = true
    private(set) var isPrisoner: Bool = false
    private(set) var isCurrentPlayer: Bool = false
    private(set) var isTurnBlock: Bool = false
    
    // MARK: - Lifecycle

    public init(name: String) {
        self.name = name
    }
    
    // MARK: - Methods

    public func getName() -> String {
        return name
    }
    
    public func set(name: String) {
        self.name = name
    }
    @discardableResult
    public func roll() -> (Int, Int) {
        return (rolling(), rolling())
    }
    
    private func rolling() -> Int {
        let dice = Dice()
        return dice.roll()
    }
    
    public func getPosition() -> Int {
        return position
    }
    
    public func setPosition(_ value: Int) {
        position = value
    }
    
    public func addToPosition(_ value: Int) {
        position += value
    }
    
    public func removeToPosition(_ value: Int) {
        position -= value
    }
    
    public func getIsFirstLaunch() -> Bool {
        return isFirstLaunch
    }
    
    public func setAsPassedFirstLaunch() {
        isFirstLaunch = false
    }
    
    public func setAs(prisoner: Bool) {
        isPrisoner = prisoner
    }
    
    public func prisonerStatus() -> Bool {
        return isPrisoner
    }
    
    public func setAs(current: Bool) {
        isCurrentPlayer = current
    }
    
    public func currentStatus() -> Bool {
        return isCurrentPlayer
    }
    
    public func setAs(turnBlock: Bool) {
        return isTurnBlock = turnBlock
    }
    
    public func turnBlockStatus() -> Bool {
        return isTurnBlock
    }
}

extension Player: Equatable {
    public static func == (lhs: Player, rhs: Player) -> Bool {
        guard lhs.uuid == rhs.uuid,
              lhs.name == rhs.name,
              lhs.squareDescription == rhs.squareDescription,
              lhs.position == rhs.position,
              lhs.isFirstLaunch == rhs.isFirstLaunch,
              lhs.isPrisoner == rhs.isPrisoner,
              lhs.isCurrentPlayer == rhs.isCurrentPlayer,
              lhs.isTurnBlock == rhs.isTurnBlock else { return false }
        return true
    }
}
