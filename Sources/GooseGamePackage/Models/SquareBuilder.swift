//
//  SquareBuilder.swift
//
//
//  Created by Massimiliano Bonafede on 23/05/24.
//

import Foundation

public class SquareBuilder {
    
    // MARK: - Properties

    private var squares: [Square] = []
    let total: Int = 63
    
    // MARK: - Lifecycle

    public init() {
        creation()
//        let bundle = Bundle.module
//        let dict = bundle.localizedInfoDictionary
//        let str = bundle.localizedString(forKey: "greating", value: nil, table: nil)
//        let string = NSLocalizedString("greating", bundle: Bundle.module, comment: "")
//        print("")
    }
    
    // MARK: - Methods

    private func creation() {
        for position in 0 ... 63 {
            switch position {
            case 0:
                let square = Square(type: .start, number: position)
                squares.append(square)
            case 5, 9, 18, 27, 36, 45, 54:
                let square = Square(type: .goose(position: position), number: position)
                squares.append(square)
            case 6:
                let square = Square(type: .bridge(position: position), number: position)
                squares.append(square)
            case 19:
                let square = Square(type: .house(position: position), number: position)
                squares.append(square)
            case 31, 52:
                let square = Square(type: .prison(position: position), number: position)
                squares.append(square)
            case 42:
                let square = Square(type: .labyrinth(position: position), number: position)
                squares.append(square)
            case 58:
                let square = Square(type: .skeleton(position: position), number: position)
                squares.append(square)
            case 63:
                let square = Square(type: .end, number: position)
                squares.append(square)
            default:
                let square = Square(type: .normal(position: position), number: position)
                squares.append(square)
            }
        }
    }
    
    public func getSquare(at index: Int) throws -> Square {
        guard let square = squares.first(where: { $0.number == index }) else { throw GameError.indexOutOfRange }
        return square
    }
}
