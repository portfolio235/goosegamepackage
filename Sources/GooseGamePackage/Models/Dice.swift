//
//  Dice.swift
//
//
//  Created by Massimiliano Bonafede on 23/05/24.
//

import Foundation

class Dice {
    
    // MARK: - Lifecycle

    init() { }
    
    // MARK: - Methods

    public func roll() -> Int {
        return Int.random(in: 1 ... 6)
    }
}
