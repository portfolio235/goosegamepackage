//
//  SquareType.swift
//
//
//  Created by Massimiliano Bonafede on 23/05/24.
//

import Foundation

public enum SquareType: Equatable {
    /// This is the initial square
    case start
    /// This square is not take any measures against the player.
    case normal(position: Int)
    /// This square move the player forward as the same number of the previous dice's roll.
    case goose(position: Int)
    /// This square makes player paid the post and he will move forward to the square number 12.
    case bridge(position: Int)
    /// This square makes player paid the post and he will stay for the next shift.
    case house(position: Int)
    /// This square makes player stopped until another player arrives in the box, which is in turn 'imprisoned.
    case prison(position: Int)
    /// This square makes player paid the post and he will move backward to the square number 39.
    case labyrinth(position: Int)
    /// This square makes player paid the post and he will move forward to the starting box.
    case skeleton(position: Int)
    /// This square makes player winner.
    case end
}
